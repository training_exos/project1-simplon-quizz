<?php
session_start();
include 'connection_bdd.php';

if (!isset($_SESSION['authenticated']) || $_SESSION['authenticated'] == 'no' )  {
    header('Location: index.php?error_connexion=Connecte toi d\'abord.');
}

/* $sql = "UPDATE resultats_quizz SET score = score + $_GET[score] WHERE themeID = $_GET[themeID] AND userID = $_GET[userID]";
 * $reponse = $bdd->query($sql);
 * $resultat = $reponse->execute();
 *  */
$sql = "SELECT * FROM resultats_quizz WHERE userID='$_SESSION[userid]'";
$reponse = $bdd->query($sql);
$resultatsQuizzUser = $reponse->fetchALL();
?>

<!DOCTYPE html>
<html lang="en">

    <head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="assets/style.css">
	<link rel="stylesheet" href="assets/tableau-des-scores">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

	<title>Le quiz des petits génies!</title>
    </head>

    <body>

	<?php include 'navbar.php'; ?>
	<div class="background-diplome">
	    <div class="container">
		<div class="row">

		    <img id="lauriers" class="col-md-3 col-sm-12 lauriers"
			 src="/assets/images/lauriers-noirs.svg" id="lauriers" alt="lauriers-noirs">

		    <div class="col-md-6 col-sm-12">

			<div id="scores">
			    
			    <h2>Tableau de tes Scores</h2>
			    <table>
				<tr>
				    <th>Thème</th>
				    <th>Score</th>
				</tr>

				<?php
				foreach ( $resultatsQuizzUser as $resultat){
				    $sql = "SELECT * FROM themes WHERE id='$resultat[themeID]'";
				    $reponse = $bdd->query($sql);
				    $theme = $reponse->fetch();

				    echo "<tr>";
				    echo "<td>" . $theme['theme'] . "</td>";
				    echo "<td>" . $resultat['score'] . "</td>";
				    echo "<tr>";
				}
				?>
			    </table>
			</div>

			<div class="graphique">
			    <?php
			    foreach ( $resultatsQuizzUser as $resultat){
				$sql = "SELECT * FROM themes WHERE id='$resultat[themeID]'";
				$reponse = $bdd->query($sql);
				$theme = $reponse->fetch();

				echo "<div style=\"background-color:blue;height:" . $resultat['score'] * 20 . "px\">";
				echo "<p>" . $theme['theme'] . "</p>";

				echo "</div>";

			    }
			    ?>
			</div>

		    </div>
		    

		    <img class="col-md-3 col-sm-12"
			 src="/assets/images/lauriers-noirs.svg" id="lauriers" alt="lauriers-noirs">
		    
		</div>
	    </div>
	</div>
	<?php include 'footer.php'; ?>

    </body>
</html>
