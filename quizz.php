<?php
session_start();
include 'connection_bdd.php';

if (!isset($_SESSION['authenticated']) || $_SESSION['authenticated'] == 'no' )  {
    header('Location: index.php?error_connexion=Connecte toi d\'abord.');
}

if (isset ($_GET['theme']))
{
    $_SESSION['theme'] = $_GET['theme'];
} elseif  (! isset($_SESSION['theme'])) {
    header('Location: choix-quizz.php?error=Choisis d\'abord un theme.');
}

?>

<!DOCTYPE html>
<html lang="fr">

    <head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="assets/style.css">
	<link rel="stylesheet" href="assets/tableau-des-scores">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>

    <body>

	<?php include 'navbar.php'; ?>

	<?php
	
	$sql = "SELECT id FROM users WHERE pseudo = '$_SESSION[nick]' AND password = '$_SESSION[pass]'";
	$reponse = $bdd->query($sql);
	$userid = $reponse->fetch();

	
	$sql = "SELECT * FROM themes WHERE theme = '$_SESSION[theme]'";
	$reponse = $bdd->query($sql);
	$theme = $reponse->fetch();


	$sql = "SELECT * FROM questions_answered WHERE userID = '$userid[0]'";
	$reponse = $bdd->query($sql);
	$numberOfQuestionsAnswered = $reponse->fetch();

	if ( $numberOfQuestionsAnswered != null)
	{
	    /* requète sql de la-mort-qui-tue ! (sélection des questions qui n'ont pas déjà été répondues) */
	    $sql = "select * from quizz_questions where theme = '$theme[theme]' 
                    AND not exists(select null from questions_answered WHERE
                    questions_answered.questionID = quizz_questions.id AND questions_answered.userID = '$userid[0]')";
	    $reponse = $bdd->query($sql);
	    $questions_to_ask = $reponse->fetchAll();
	} else {
	    $sql = "SELECT * FROM quizz_questions WHERE theme = '$theme[theme]'";
	    $reponse = $bdd->query($sql);
	    $questions_to_ask = $reponse->fetchAll();
	}
	$randomQuestionIndex = rand(0,sizeof($questions_to_ask) - 1);
	?>

	<div class="container-corps-quizz">

	    <div class="theme">
		<img alt="image du thème" src="<?php echo $theme['image_path'] ?>"/>
	    </div>

	    <?php if ($questions_to_ask != null): ?>
		<?php $themeDone = 0 ?>
		<div class="container-global-questions">
		    <div class="compteur-questions">
			<p>
			    Question <?=6 - sizeof($questions_to_ask)?> sur <?=5?>
			</p>

			<div id="timer">
			</div>
		    </div>

		    <div id="question">
			<div>
			    <p><?php echo $questions_to_ask[$randomQuestionIndex]['question_content'];?></p>
			</div>
			
			<div>
			    <form action=""  method="POST">
				<!-- type = button pour pas que ça submit par défaut -->
				<button type="button" class="btn btn-secondary btquizz"
					name="<?=$questions_to_ask[$randomQuestionIndex]['prop1']?>">
				    <?= $questions_to_ask[$randomQuestionIndex]['prop1'];?></button>

				<button type="button" class="btn btn-secondary btquizz"
					name="<?=$questions_to_ask[$randomQuestionIndex]['prop2'];?>">
				    <?= $questions_to_ask[$randomQuestionIndex]['prop2'];?></button>

				<button type="button" class="btn btn-secondary btquizz"
			    		name="<?=$questions_to_ask[$randomQuestionIndex]['answer'];?>">
				    <?= $questions_to_ask[$randomQuestionIndex]['answer'];?></button>

				<button type="submit" name="submit" class="button-inactive"
					      value="<?=$questions_to_ask[$randomQuestionIndex]['id']?>" disabled>
				    Question suivante
				</button>
			    </form>
			</div>
			
		    </div>
		</div>

	    <?php endif; ?>

	    <?php if ($questions_to_ask == null): ?>
		<div class="fin-theme">
		    <p >
			Tu as fini le thème ! Choisis un <a href="/choix-quizz.php">autre thème</a>
			ou bien va voir <a href="tableau-des-scores.php">les résultats</a>. </p>
		</div>
		<?php
		$themeDone = 1;

		$sql = "UPDATE resultats_quizz SET done = $themeDone WHERE themeID = $theme[id] AND userID = $userid[0]";
		$reponse = $bdd->query($sql);
		$resultat = $reponse->execute();
		?>
	    <?php endif; ?>

	</div>

	<?php include 'footer.php'; ?>


	<?php
	$userIDjs = htmlspecialchars($userid[0]);
	$themeIDjs = htmlspecialchars($theme['id']);
	$themeDonejs = htmlspecialchars($themeDone);
	?>
	
	<script type="text/javascript">
	 let userID = "<?php echo $userIDjs; ?>";
	 let themeID = "<?php echo $themeIDjs; ?>";
	 let themeDone = "<?php echo $themeDonejs; ?>";
	</script>

	<script src="assets/scripts/main.js"></script>

    </body>

</html>
