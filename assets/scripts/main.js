// Let's randomize the good answer's button position (easy-peasy ;)
var buttonGoodAnswer = document.querySelector("#question div:nth-child(2) form button:nth-child(3)");
var randomRow = Math.floor((Math.random() * 3) + 1);
buttonGoodAnswer.style.gridRow = randomRow + "/" + randomRow;

var buttonsAnswer = document.querySelectorAll(".btquizz");
var buttonSubmit = document.querySelector("[name=submit]");
var questionID = buttonSubmit.getAttribute("value");
// userid, themeID & themeDone variables are set in quizz.php


buttonsAnswer.forEach((button) => {
  button.addEventListener('click', () => {
      var answer = button.getAttribute("name");

      isRightAnswer(answer,questionID).then(function(response) {
	  if ( response == "true") {

	      fetch("/score_insertion.php?score=1" + "&userID=" + userID + "&themeID=" + themeID + "&done=" + themeDone + "&questionID="  + questionID);
	      button.classList.add("btn-success");
	      
	  } else {
	      fetch("/score_insertion.php?score=0" + "&userID=" + userID + "&themeID=" + themeID + "&done=" + themeDone + "&questionID="  + questionID);
	      button.classList.add("btn-danger");
	  }

	  document.querySelector('.button-inactive').disabled = false;
	  document.querySelector('.button-inactive').style.color = "green";
	  document.querySelector('.button-inactive').style.opacity = "1";

	  buttonsAnswer.forEach((button) => {
	      button.disabled = true;
	  })
      })

      
  });
});

function isRightAnswer(answer,questionID)
{
    return fetch("/check_answer.php?answer=" + answer + "&questionID=" + questionID )
	.then(function(response) {
	    return response.text();
	})
	.then(function(mytext) {
	    return mytext;
	});
}


// TIMER
// The code below was largely copy-pasted from a web page, but I had not much time left (hey but everything else is hand-made! :)

// console.logs in order to understand how things works
// console.log( new Date("Jan 5, 2021 15:37:25").getTime() );
// console.log( Date.now() + 5000 );

// Set the date we're counting down to
// var countDownDate = new Date("Jan 5, 2021 15:37:25").getTime();
var countDownDate = Date.now() + 15000;
    
// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();

  // Find the distance between now and the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="timer"
  // document.getElementById("timer").innerHTML = days + "d " + hours + "h "
    // + minutes + "m " + seconds + "s ";
    document.getElementById("timer").innerHTML =  seconds + " Secondes restantes";

  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
      document.getElementById("timer").innerHTML = "C'est fini ! :( Tu as été trop long-ue à répondre, je t'ai mis 0, désolé";
      fetch("/score_insertion.php?score=0" + "&userID=" + userID + "&themeID=" + themeID + "&done=" + themeDone + "&questionID="  + questionID);

      document.querySelector('.button-inactive').disabled = false;
      document.querySelector('.button-inactive').style.color = "green";
      document.querySelector('.button-inactive').style.opacity = "1";


      buttonsAnswer.forEach((button) => {
	  button.disabled = true;
      })
  }
}, 1000);

