<?php
session_start();
session_destroy();
header("Refresh: 5;URL=index.php");

?>

<p>
    Tu as bien été déconnecté. A bientôt !
</p>

<p>
    Retour automatique à l'accueil dans 5 secondes
</p>

<small>
    <a href="index.php">Clique ici</a> pour revenir à l'accueil tout de suite;    
</small>

