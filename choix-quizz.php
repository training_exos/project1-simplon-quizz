<?php
session_start();
include 'connection_bdd.php';

if (isset($_SESSION['authenticated'])) {
    if ($_SESSION['authenticated'] != 'yes') {
        if (!isset($_POST['nick']) or (!isset($_POST['password']))) {
            header('Location: index.php?error_connexion=Connect first please');
        }
        $_SESSION['nick'] = $_POST['nick'];
        $_SESSION['pass'] = $_POST['password'];
        $nick = $_SESSION['nick'];
        $pass = $_SESSION['pass'];

	/* Connection from an existing account*/
        if (isset($_POST['connexion'])) {
            $sql = "SELECT * FROM users WHERE pseudo = '$nick' AND password = '$pass'";
            $reponse = $bdd->query($sql);
            $user = $reponse->fetch();

            if ($user == null) {
                $_SESSION['authenticated'] = 'no';
                header('Location: index.php?error_connexion=Mauvais pseudo ou mot de passe');
            } else {
                $_SESSION['userid'] = $user['id'];
                $_SESSION['authenticated'] = 'yes';
            }
        } else { /* Account creation - availability check */
            $sql = "SELECT * FROM users WHERE pseudo = '$nick'";
            $reponse = $bdd->query($sql);
            $user = $reponse->fetch();
            if ($user != null) {
                $_SESSION['authenticated'] = 'no';
                header('Location: index.php?error_creation=Ce pseudo est pris, désolé');
            } else { /* Account Creation */
                $req = $bdd->prepare('INSERT INTO users(pseudo, password) VALUES(:pseudo, :password)');

                $req->execute(array(
                    'pseudo' => $nick,
                    'password' => $pass
                ));

                $sql = "SELECT * FROM users WHERE pseudo = '$nick' AND password = '$pass'";
                $reponse = $bdd->query($sql);
                $user = $reponse->fetch();
                $_SESSION['userid'] = $user['id'];

                $_SESSION['authenticated'] = 'yes';

		/* Creation of resultats_quizz table default entries */
		for ($i=1; $i<6; $i++) {

		    $req = $bdd->prepare('INSERT INTO resultats_quizz(userID, themeID) VALUES(:userID, :themeID)');

		    $req->execute(array(
			'userID' => $user[0],
			'themeID' => $i
		    ));
		}

	    }
	}
    }
} else {
    header('Location: index.php?error_creation=Merci de vous connecter en premier');
}

$sql = "SELECT * FROM themes LEFT JOIN resultats_quizz ON resultats_quizz.themeID = themes.id
        AND resultats_quizz.userID = $_SESSION[userid] WHERE resultats_quizz.done IS FALSE";

$reponse = $bdd->query($sql);
$themesNotDone = $reponse->fetchAll();
?>

<!DOCTYPE html>
<html lang="fr">
    <head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
	      integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<link rel="stylesheet" href="assets/style.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </head>
    <body>
	<?php include 'navbar.php';?>
	
	<div class="background-accueil2">
            <div class="container">
		<div class="d-flex justify-content-center">
                    <?php if ($themesNotDone != null): ?>
			<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" data-interval="2000">

			    <div class="carousel-inner" role="listbox">
		    <?php endif; ?>
		    
		    <?php
		    $i = 0;
		    if ($themesNotDone != null)
		    {
			foreach ($themesNotDone as $theme) {
			    if ( $i == 0 )
			    {
				echo "
                                       <div class=\"carousel-item active\">
                                          <h2>$theme[theme]</h2>
                                          <a href=\"quizz.php?theme=$theme[theme]\">
                                          <img class=\"d-block\" src=\"$theme[image_path]\" alt=\"theme $theme[theme]\"/>
                                          </a>
                                       </div>"; } else {
					   echo "
                                                <div class=\"carousel-item\">
                                                   <h2>$theme[theme]</h2>
                                                   <a href=\"quizz.php?theme=$theme[theme]\">
                                                       <img class=\"d-block\" src=\"$theme[image_path]\" alt=\"theme $theme[theme]\" />
                                                   </a>
                                                </div>";
				       }
			    $i++;
			}
		    }
		    ?>

		    <?php if ($themesNotDone != null): ?>
			    </div>

			    <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			    </a>

			    <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			    </a>

			    <ol class="carousel-indicators">;
				<?php

				for ($e=0; $e<$i; $e++) {
				    echo "<li data-target=\"#carouselExampleIndicators\" data-slide-to=\"$e\" class=\"active\"></li>";
				}
				?>
			    </ol>

		    <?php endif; ?>

		    <?php
		    if ($themesNotDone == null)
		    {
			echo '<div  class="fin-theme-tous">
<div><p>Tu as tout fini !</p></div>
 <button> <a href="/tableau-des-scores.php">Voir mes résultats</a></button>
			</div>';
		    }
		    ?>

			</div>
			
		</div>
	    </div>
	    <?php include 'footer.php'; ?>
    </body>
</html>
